package lab.publisher.web;


import lab.publisher.data.PublisherRepository;
import lab.publisher.model.Publisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
public class PublisherRest {

    private final PublisherRepository publisherRepository;

    @Value("${log.prefix:no-prefix}")
    private String logPrefix;

    @GetMapping("/publishers")
    public List<Publisher> getPublishers() {
        log.info("[{}] about to retrieve all publishers", logPrefix);
        return publisherRepository.findAll();
    }

    @GetMapping("/publishers/{id}")
    public Optional<Publisher> getPublisherById(@PathVariable("id") int id) {
        log.info("[{}] about to retrieve publisher {}", logPrefix, id);
        return publisherRepository.findById(id);
    }

    @PostMapping("/publishers")
    public Publisher addPublisher(@RequestBody Publisher p) {
        log.info("[{}] about to add publisher {}", logPrefix, p);
        publisherRepository.save(p);
        return p;
    }
}
