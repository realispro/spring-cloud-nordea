import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AppSettings} from "../../app.settings";
import {BehaviorSubject} from "rxjs";
import {GridDataResult} from "@progress/kendo-angular-grid";
import {Cart} from "../model/cart.model";

@Injectable()
export class CartService extends BehaviorSubject<GridDataResult> {

  cart: Cart;

  constructor(private httpClient: HttpClient) {
    super(null);
    this.create().subscribe(result=>this.cart=result);
  }

  public create() {
    return this.httpClient.post(AppSettings.CART_ENDPOINT, new Object());
  }

  public add(bookId: number) {
    let url: string = `${AppSettings.CART_ENDPOINT}/${this.cart.id}/items/${bookId}`;
    //console.log('connecting to ' + url);
    this.httpClient.put(url, 1).subscribe(result=>this.refresh());
    //console.log("cart items: " + this.cart.items)
  }

  public remove(bookId: number) {
    let url: string = `${AppSettings.CART_ENDPOINT}/${this.cart.id}/items/${bookId}`;
    //console.log('connecting to ' + url);
    this.httpClient.delete(url).subscribe(result=>this.refresh());

    //console.log("cart items: " + this.cart.items)
  }

  public confirm(){
    let url: string = `${AppSettings.CART_ENDPOINT}/${this.cart.id}`;
    //console.log('connecting to ' + url);
    this.httpClient.post(url, new Object()).subscribe(result=>this.cart=result);
  }

  public refresh() {
    let url: string = `${AppSettings.CART_ENDPOINT}/${this.cart.id}`;
    //console.log('connecting to ' + url);
    this.httpClient.get(url).subscribe(result=>this.cart.items=result["items"]);
  }
}
