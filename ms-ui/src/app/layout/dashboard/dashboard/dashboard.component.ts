import {Component, OnInit} from '@angular/core';
import {PublisherService} from '../../../shared/service/publisher.service';
import 'hammerjs';
import {Publisher} from "../../../shared/model/publisher.model";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public chartTitle: any = { text: 'Order Chart' };
  public publishers: Publisher[];

  constructor(private publisherService: PublisherService) { }

  ngOnInit(): void {
    this.publisherService.fetchAll().subscribe(result => {
      this.publishers=result['content'];
    });
  }



}
