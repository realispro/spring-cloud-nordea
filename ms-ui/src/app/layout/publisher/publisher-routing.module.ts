import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PublisherComponent} from './publisher/publisher.component';
import {PublisherViewComponent} from './publisher-view/publisher-view.component';
import {PublisherAddComponent} from './publisher-add/publisher-add.component';
import {PublisherEditComponent} from './publisher-edit/publisher-edit.component';

const routes:Routes=[
  {
    path:'',
    component:PublisherComponent
  },
  {
    path:'new',
    component:PublisherAddComponent
  },
  {
    path:':id/view',
    component:PublisherViewComponent
  },
  {
    path:':id/edit',
    component:PublisherEditComponent
  }
]
@NgModule({

  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublisherRoutingModule { }
