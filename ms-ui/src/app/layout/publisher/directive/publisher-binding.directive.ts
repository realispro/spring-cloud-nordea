import {Directive, OnDestroy, OnInit} from "@angular/core";
import {DataBindingDirective, GridComponent} from "@progress/kendo-angular-grid";
import {PublisherService} from "../../../shared/service/publisher.service";
import {takeUntil} from 'rxjs/operators';
import {Subject} from "rxjs";

@Directive({
    selector: '[publisherBinding]'
})
export class PublisherBindingDirective extends DataBindingDirective implements OnInit, OnDestroy {

    private _$alives = new Subject();

    constructor(grid: GridComponent,
        private departmentService: PublisherService) {
        super(grid);
    }

    public ngOnInit(): void {
        this.departmentService.pipe(takeUntil(this._$alives)).subscribe(
            (result) => {
                this.grid.loading = false;
                this.grid.data = result;
                this.notifyDataChange();
            }
        );
        super.ngOnInit();
        this.rebind();
    }

    public ngOnDestroy(): void {
        this._$alives.next();
        this._$alives.complete();
        super.ngOnDestroy();
    }

    public rebind(): void {
        this.grid.loading = true;
        this.departmentService.query(this.state);
    }

}


