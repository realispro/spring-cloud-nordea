import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {BookService} from '../../../shared/service/book.service';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Model} from '../../../shared/model/book.model';

@Component({
  selector: 'app-employee-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.css']
})
export class BookViewComponent implements OnInit, OnDestroy {

  private _$alive = new Subject();
  public book:Model;

  constructor(private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _location: Location,
    private _bookService: BookService) { }

  ngOnInit(): void {
    this._activatedRoute.params.pipe(takeUntil(this._$alive)).subscribe(params => {
      let id = params['id'];
      this._bookService.fetchById(id).subscribe(result => this.book=result);
    });
  }

  public back() {
    this._location.back();
  }

  ngOnDestroy() {
    this._$alive.next();
    this._$alive.complete();
  }

}
