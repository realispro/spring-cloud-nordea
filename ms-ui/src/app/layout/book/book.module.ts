import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BookComponent} from './book/book.component';
import {BookRoutingModule} from './book-routing.module';
import {GridModule} from '@progress/kendo-angular-grid';
import {EmployeeBindingDirective} from './directive/employee-binding.directive';
import {BookViewComponent} from './book-view/book-view.component';
import {BookAddComponent} from './book-add/book-add.component';
import {BookEditComponent} from './book-edit/book-edit.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [BookComponent, EmployeeBindingDirective, BookViewComponent, BookAddComponent, BookEditComponent],
  imports: [
    CommonModule,
    BookRoutingModule,
    GridModule,
    ReactiveFormsModule

  ]
})
export class BookModule { }
