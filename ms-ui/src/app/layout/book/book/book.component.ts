import {Component, OnInit} from '@angular/core';
import {BookService} from '../../../shared/service/book.service';
import {Router} from '@angular/router';
import {CartService} from "../../../shared/service/cart.service";

@Component({
  selector: 'app-employee',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  constructor(
    private _router: Router,
    private _bookService: BookService,
    private _cartService: CartService) { }

  ngOnInit(): void {
  }

  public goToNewBookView() {
    this._router.navigate(['book','new']);
  }

  public delete(id:string){
    this._bookService.delete(id).subscribe(result => {
      this._bookService.query({});
    });
  }

  public buy(id:number){
    console.log('adding book ' + id);
    this._cartService.add(id);
  }

}
