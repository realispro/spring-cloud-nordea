package lab.delivery.model;

import lombok.Data;

import java.util.List;

@Data
public class Cart {

    private long id;
    private List<Item> items;
    private float totalPrice;
}
