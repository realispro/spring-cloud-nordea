package lab.delivery.model;

import lombok.Data;

@Data
public class Item {

    private int productId;
    private String productName;
    private float price;
    private int amount;
}
