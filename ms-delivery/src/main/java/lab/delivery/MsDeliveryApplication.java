package lab.delivery;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lab.delivery.model.Cart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;

@SpringBootApplication
@Slf4j
public class MsDeliveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsDeliveryApplication.class, args);
    }


    @Bean
    Consumer<String> consume(){
        return cartString -> {
            try {
                Cart cart = new ObjectMapper().readValue(cartString, Cart.class);
                log.info("cart received: {}", cart);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        };
    }


}
