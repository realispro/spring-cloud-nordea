package lab.book.service;

import lab.book.model.Rating;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "rating-service", fallbackFactory = RatingFallbackFactory.class)
public interface RatingService {

    @GetMapping("/ratings")
    List<Rating> getRatingsForBooks(@RequestParam("bookId") int bookId);
}
