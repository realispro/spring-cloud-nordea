package lab.book.service;

import lab.book.model.Publisher;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

//@Service
@Slf4j
@RequiredArgsConstructor
public class SimplePublisherService implements PublisherService {

    private final RestTemplate restTemplate;

    @Override
    //@io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker(name = "publisher-service-cb", fallbackMethod = "getPublisherFallback")
    public Publisher getPublisher(int id) {
        log.info("about to retrieve publisher {}", id);
        String publisherServiceUrl = "http://publisher-service";
        log.info("connecting to {}", publisherServiceUrl);

        return restTemplate.exchange(
                publisherServiceUrl + "/publishers/" + id,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                Publisher.class).getBody();

    }

    // do not invoke directly!
    private Publisher getPublisherFallback(int id, Throwable t) {
        log.error("problem while connecting to publisher service", t);
        if(t instanceof Error){
            throw (Error)t;
        }
        return null;
    }
}
