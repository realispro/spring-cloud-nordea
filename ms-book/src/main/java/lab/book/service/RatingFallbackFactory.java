package lab.book.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RatingFallbackFactory implements FallbackFactory<RatingService> {
    @Override
    public RatingService create(Throwable cause) {
        log.error("An exception occurred when calling the RatingService", cause);
        return id -> List.of();
    }
}
