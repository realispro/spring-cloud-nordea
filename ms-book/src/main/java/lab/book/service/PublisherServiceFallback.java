package lab.book.service;

import lab.book.model.Publisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PublisherServiceFallback implements PublisherService{
    @Override
    public Publisher getPublisher(int id) {
        log.error("fallback method for getPublisher(" + id + ")");
        return null;
    }
}
