package lab.book.service;

import lab.book.model.Rating;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

//@Service
@RequiredArgsConstructor
public class RatingServiceBean implements RatingService {
    private final RestTemplate restTemplate;
    private static final String RATING_URL = "http://rating-service";

    @Override
    public List<Rating> getRatingsForBooks(int bookId) {
        Rating[] ratings =  restTemplate.exchange(
                RATING_URL + "/ratings?bookId=" + bookId,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                Rating[].class).getBody();
        return Arrays.asList(ratings);
    }
}
