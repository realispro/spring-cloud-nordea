package lab.book.web;


import lab.book.data.BookRepository;
import lab.book.model.Book;
import lab.book.model.Publisher;
import lab.book.model.Rating;
import lab.book.service.PublisherService;
import lab.book.service.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@Slf4j
@RefreshScope
public class BookRest {

    private final BookRepository bookRepository;
    private final PublisherService publisherService;
    private final RatingService ratingService;

    @Value("${log.prefix:no-prefix}")
    private String logPrefix;

    @GetMapping("/books")
    public List<Book> getBooks() {
        log.info("[{}] about to retrieve all books", logPrefix);
        return bookRepository.findAll().stream().peek(this::fillRating).collect(Collectors.toList());
    }

    @GetMapping("/books/{id}")
    public ResponseEntity getBookById(@PathVariable("id") int id) {
        log.info("[{}] about to retrieve book {}", logPrefix, id);
        Optional<Book> book = bookRepository.findById(id);
        if (book.isPresent()) {
            Book b = book.get();
            fillRating(b);
            return ResponseEntity.ok(b);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    private void fillRating(Book b) {
        // Rating service interface
        // rating service implementation
        // Rating data object
        // invoke service
        // calculate average rating

        List<Rating> ratings = ratingService.getRatingsForBooks(b.getId());

        double average = ratings.stream()
                .mapToDouble(Rating::getRate).average().orElse(0);
        b.setRating(average);

    }

    @PostMapping("/books")
    public ResponseEntity addBook(@RequestBody Book b) {
        log.info("[{}] about to add book {}", logPrefix, b);
        Publisher publisher = publisherService.getPublisher(b.getPublisherId());
        if (publisher != null) {
            bookRepository.save(b);
            return ResponseEntity.status(HttpStatus.CREATED).body(b);
        } else {
            return ResponseEntity.badRequest().body("publisher is missing");
        }
    }
}
