package lab.book.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    @ManyToOne
    private Author author;
    private String cover;
    private float price;
    private Integer publisherId;
    @Transient
    private double rating;
}
