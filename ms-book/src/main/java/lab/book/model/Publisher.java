package lab.book.model;

import lombok.Data;

@Data
public class Publisher {

    private Integer id;
    private String name;
    private String logo;

}
