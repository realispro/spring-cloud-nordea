package lab.book.model;

import lombok.Data;


@Data
public class Rating {

    private Integer id;
    private int rate;
    private String description;
    private int bookId;
}
