package lab.ratings.web;

import lab.ratings.data.RatingRepository;
import lab.ratings.model.Rating;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RatingsRest {

    private final RatingRepository ratingRepository;

    @GetMapping("/ratings")
    public List<Rating> getRatingsByBook(@RequestParam("bookId") int bookId){
        log.info("fetching ratings for a book {}", bookId);
        return ratingRepository.findAllByBookId(bookId);
    }
}
